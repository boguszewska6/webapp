**Urochomienie aplikacji**

Stworzenie maszyny wirtualnej


Logowanie się na serwer i dodanie klucza ssh

```bash
  ssh-add ~/.ssh/ssh.pem
  ssh ubuntu@20.219.112.75
```

Tworzenie katalogu i skopiowanie repozytorium

```bash
  mkdir /var/www
  cd /var/www
  git clone git@bitbucket.org:boguszewska6/webapp.git
```

Zainstalowanie paczek

```bash
  curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -
  sudo apt-get update
  sudo apt-get install -y nodejs nginx
  sudo npm install -g pm2
```


Urochomienie frontu

```bash
  cd /var/www/webapp/front
  npm ci
  npm run build
```

Urochomienie backandu
```bash
  cd /var/www/webapp
  npm ci
  pm2 start app.js
```