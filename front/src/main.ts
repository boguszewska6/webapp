import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import io from 'socket.io-client';


createApp(App).use(router).mount('#app')


const socket = io('https://3000-boguszewska6-webapp-tmbu06ap89i.ws-eu38.gitpod.io/', {
    reconnection: false,
    transports: ["websocket", "polling"]
});
